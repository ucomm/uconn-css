//TODO change placeholder attr



(function() {

	/// Basic selector function
	function _(sel, ctx) { var findings = (ctx || document).querySelectorAll(sel); return findings.length > 1 ? findings : findings[0];}

	window.addEventListener('load', function() {

		//Collect Components
		var searchButton = _('.uc_banner__button--search'),
				searchForm = _('.uc_banner__searchForm'),
				searchField = _('.uc_banner__searchBar')
				dropdownButton = _('.uc_banner__dropdownButton'),
				dropdownBody = _('.uc_banner__dropdownBody'),
				dropdownElements = _('.uc_banner__dropdownElement'),
				searchTarget = _('.uc_banner__dropdownElement--selected');


		if(searchForm) {
			//Control dropdown functionality
			dropdownButton.addEventListener('click', function() {
				dropdownButton.toggleClass('active');
			}, false);

			//Control search button functionality
			searchButton.addEventListener('click', function(e) {

				if(searchForm.hasClass('active')) { //If the search bar is open
					if(searchField.value != '') {
						//Set up the href attribute of the link so that we go to the right place
						var engine = searchTarget.dataset.searchEngine;
						if(engine == 'google') {
							var searchParams = searchTarget.dataset.searchParams.replace(/%/g, '\\%');
							searchButton.href = searchTarget.dataset.searchUrl+'?q='+searchField.value.replace(/ /g, '+')+'&'+searchParams;
						} else if(engine == 'wordpress') {
							searchButton.href = searchTarget.dataset.searchUrl+'?s='+searchField.value;
						} else if(engine == 'drupal') {
							searchButton.href = searchTarget.dataset.searchUrl+searchField.value;
						}

					} else {
						searchForm.removeClass('active');
						dropdownButton.removeClass('active');
						e.preventDefault();
					}
				} else { //Otherwise, open the search bar
					searchForm.addClass('active');
					dropdownButton.removeClass('active');
					e.preventDefault();
				}


			}, false);

			searchField.addEventListener('keydown', function(e) {
				console.log('keydown');
				if(e.keyCode == 13) {//Return
					if (searchButton.fireEvent) {
				    searchButton.fireEvent('onclick');
				  } else {
				    var evObj = document.createEvent('Events');
				    evObj.initEvent('click', true, false);
				    searchButton.dispatchEvent(evObj);
				  }
				}
			}, false);

			dropdownElements.addEventListener('click', function() {
				searchTarget = this;
				_('.uc_banner__dropdownElement--selected').removeClass('uc_banner__dropdownElement--selected');
				this.addClass('uc_banner__dropdownElement--selected');
				searchField.placeholder = 'Search ' + this.innerHTML;
			}, false);


			window.addEventListener('click', function(e) {

				if(e.target.getClosest('.uc_banner__dropdownButton') == null) {
					dropdownButton.removeClass('active');
				}

			}, false);
		}

	});

	//Helper function to simulate events
	function eventFire(el, etype){
	  if (el.fireEvent) {
	    el.fireEvent('on' + etype);
	  } else {
	    var evObj = document.createEvent('Events');
	    evObj.initEvent(etype, true, false);
	    el.dispatchEvent(evObj);
	  }
	}

	NodeList.prototype.addEventListener = function(_event, _method, _capture) {
		for (var i = 0; i < this.length; i++) {
			this[i].addEventListener(_event, _method, (_capture || true));
		}
	};


	NodeList.prototype.toggleClass = function(_class) {
		for (var i = 0; i < this.length; i++) {
		  this[i].toggleClass(_class);
		}
	};
	Element.prototype.toggleClass = function(_class) {

			if(this.hasClass(_class)) {

				this.removeClass(_class);

			} else {

				this.addClass(_class);

			}
	};

	NodeList.prototype.addClass = function(_class) {
		for (var i = 0; i < this.length; i++) {
		  this[i].addClass(_class);
		}
	}
	Element.prototype.addClass = function(_class) {
		
		if(this.classList) {//ClassList Support

			this.classList.add(_class);

			return this;

		} else {//Fallback to regex

			var classes = this.className.split(' ');

			if(classes.indexOf(classToAdd) === -1) {

				this.className = this.className + (classes.length > 0 ? ' ' : '') + classToAdd;

			}
			
			return this;
		}
	};

	NodeList.prototype.removeClass = function(_class) {
		for (var i = 0; i < this.length; i++) {
		  this[i].removeClass(_class);
		}
	}
	Element.prototype.removeClass = function(_class) {

		if(this.classList) {

			this.classList.remove(_class);

			return this;

		} else {

			var finalClassName = '';

			this.className.split(' ').forEach(function(cl) {

				if(cl != _class) { finalClassName += cl + ' ' }

			});

			this.className = finalClassName.replace(/[ /t]+$/, '');

			return this;	

		}
	};

	NodeList.prototype.hasClass = function(_class) {
		for (var i = 0; i < this.length; i++) {
		  this[i].hasClass(_class);
		}
	}
	Element.prototype.hasClass = function(_class) {
		if(this.classList) {

			return this.classList.contains(_class);

		} else {

			return this.className.split(' ').indexOf(_class) != -1;

		}
	};

	NodeList.prototype.getClosest = function (sel) {
		for (var i = 0; i < this.length; i++) {
			this[i].getClosest(sel);
		}
	}
	Element.prototype.getClosest = function ( sel ) {

	    // Variables
	    var firstChar = sel.charAt(0);
	    var supports = 'classList' in document.documentElement;
	    var attribute, value;
	    var el = this;

	    // If sel is a data attribute, split attribute from value
	    if ( firstChar === '[' ) {
	        sel = sel.substr( 1, sel.length - 2 );
	        attribute = sel.split( '=' );

	        if ( attribute.length > 1 ) {
	            value = true;
	            attribute[1] = attribute[1].replace( /"/g, '' ).replace( /'/g, '' );
	        }
	    }

	    // Get closest match
	    for ( ; el && el !== document && el.nodeType === 1; el = el.parentNode ) {

	        // If sel is a class
	        if ( firstChar === '.' ) {
	            if ( supports ) {
	                if ( el.classList.contains( sel.substr(1) ) ) {
	                    return el;
	                }
	            } else {
	                if ( new RegExp('(^|\\s)' + sel.substr(1) + '(\\s|$)').test( el.className ) ) {
	                    return el;
	                }
	            }
	        }

	        // If sel is an ID
	        if ( firstChar === '#' ) {
	            if ( el.id === sel.substr(1) ) {
	                return el;
	            }
	        }

	        // If sel is a data attribute
	        if ( firstChar === '[' ) {
	            if ( el.hasAttribute( attribute[0] ) ) {
	                if ( value ) {
	                    if ( el.getAttribute( attribute[0] ) === attribute[1] ) {
	                        return el;
	                    }
	                } else {
	                    return el;
	                }
	            }
	        }

	        // If sel is a tag
	        if ( el.tagName.toLowerCase() === sel ) {
	            return el;
	        }

	    }

	    return null;
	};

})();
