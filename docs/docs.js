var data = {
	"blocks": [
		{
			"name": "Color Classes",
			"description": "Sets the specified color property to the specified color.",
			"markup": {
				"example": "@markup .uc_[COLOR NAME]-[COLOR TYPE]",
				"escaped": "@markup .uc_[COLOR NAME]-[COLOR TYPE]"
			},
			"state": [
				{
					"name": ".uc_[COLOR NAME]-c",
					"escaped": "uc_[COLOR NAME]-c",
					"description": "sets the color to COLOR NAME."
				},
				{
					"name": ".uc_[COLOR NAME]-bg",
					"escaped": "uc_[COLOR NAME]-bg",
					"description": "sets the background-color to COLOR NAME."
				},
				{
					"name": ".uc_[COLOR NAME]-bd",
					"escaped": "uc_[COLOR NAME]-bd",
					"description": "sets the border-color to COLOR NAME."
				}
			]
		},
		{
			"name": "Gradient Classes",
			"description": "Sets the background-image property to the specified gradient.",
			"markup": {
				"example": "@markup .uc_[COLOR A]-to-[COLOR B]",
				"escaped": "@markup .uc_[COLOR A]-to-[COLOR B]"
			},
			"state": {
				"name": ".uc_[COLOR A]-to-[COLOR B]--reverse",
				"escaped": "uc_[COLOR A]-to-[COLOR B]--reverse",
				"description": "Reverses the order of the colors in the gradient."
			}
		}
	]
};